﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipInput : MonoBehaviour {
	private Vector2 axis;

	public PlayerBehaviour ship;
	public Weapons weapons;

	
	// Update is called once per frame
	void Update () {
		axis.x = Input.GetAxis ("Horizontal");
		axis.y = Input.GetAxis ("Vertical");

		ship.SetAxis (axis);

		if (Input.GetButton ("Fire1")) {
            
            weapons.ShotWeapon();
		}
	}
}

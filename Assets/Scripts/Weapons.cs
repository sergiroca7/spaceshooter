﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour {
   
    private Cartridge cart;
    public Cannon cannon;
    
    public int maxBullets;
    public Transform bulletPosition;

    public void ShotWeapon()
    {
        cannon.ShootCannon(cart.GetBullet(), transform.position, 0f);
    }

    private void Start()
    {
       GameObject bullet = Resources.Load("Prefabs/Bullet") as GameObject;
        cart = new Cartridge(bullet, maxBullets, bulletPosition);
    }
    


}

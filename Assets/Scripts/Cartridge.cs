﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cartridge
{

    private Bullet[] bullets;
    private int currentBullet = 0;

    public Cartridge(GameObject bullet, int maxBullets, Transform bulletPosition)
    {
        bullets = new Bullet[maxBullets];
        for(int i = 0; i < maxBullets; i++)
        {
            Vector2 spawnPos = bulletPosition.position;
            spawnPos.x -= i * 0.2f;
            spawnPos.y -= i * 0.2f;
            GameObject tmpbullet = GameObject.Instantiate(bullet, spawnPos, Quaternion.identity, bulletPosition);
            tmpbullet.name = "Bullet_" + i;

            bullets[i] = tmpbullet.GetComponent<Bullet>();
        }
        

    }

    public Bullet GetBullet()
    {
        if(currentBullet > bullets.Length - 1)
        {
            currentBullet = 0;
        }

        return bullets[currentBullet++];
    }

}
